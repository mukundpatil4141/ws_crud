package com.javainterviewpoint.dao;

import java.util.List;

import com.javainterviewpoint.model.Student;

public interface StudentDao {

	public void createStudent(Student student);

	public List<Student> getAllStudent();

	public Student getStudentById(int studentId);

	public void deleteStudent(int studentId);
	
	public void updateStudent(Student student);
}
