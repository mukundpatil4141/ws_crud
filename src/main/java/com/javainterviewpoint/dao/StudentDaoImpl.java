package com.javainterviewpoint.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.javainterviewpoint.model.Student;

@Repository
public class StudentDaoImpl implements StudentDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void createStudent(Student student) {

		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		s.save(student);
		tx.commit();
		s.close();

	}

	@Override
	public List<Student> getAllStudent() {

		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		List<Student> studentList = (ArrayList<Student>)s.createCriteria(Student.class).list();
		tx.commit();
		s.close();
		return studentList;
	}

	@Override
	public Student getStudentById(int studentId) {

		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		Student student = (Student)s.get(Student.class,studentId);
		tx.commit();
		s.close();
		
		return student;
		
	}

	@Override
	public void deleteStudent(int studentId) {
		
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		Student student = s.load(Student.class, studentId);
		s.delete(student);
		tx.commit();
		s.close();
	}

	@Override
	public void updateStudent(Student student) {
		
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		Student studentUpdated = s.load(Student.class, student.getId());
		s.update(student);
		tx.commit();
		s.close();
		
	}

}
