package com.javainterviewpoint.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.javainterviewpoint.dao.StudentDao;
import com.javainterviewpoint.model.Student;

@RestController
@JsonFormat
public class StudentController {

	@Autowired
	StudentDao studentDao;

	/* Creating a new student */

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public void createStudent(@RequestBody Student student) {
		System.out.println("pppppppppppppppp");
		System.out.println("=====" + student.getId() + " " + student.getName()
				+ " " + student.getAge());
		studentDao.createStudent(student);
	}

	
	/* getting all student record */

	@RequestMapping(value = "/listStudent", method = RequestMethod.GET)
	public ResponseEntity getListStudent() {

		List studentList = studentDao.getAllStudent();
		if (studentList == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity(studentList, HttpStatus.OK);
		}

	}
	
	
	/* get single student record */

	@RequestMapping(value = "/getSingleRecord/{studentId}", method = RequestMethod.GET)
	public ResponseEntity getStudent(@PathVariable int studentId) {

		Student studentRecord = studentDao.getStudentById(studentId);
		if (studentRecord == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity(studentRecord, HttpStatus.FOUND);
		}

	}
	
	
	/* delete single student record */
	
	@RequestMapping(value = "/deleteRecord/{studentId}", method = RequestMethod.DELETE)
	public ResponseEntity deleteStudent(@PathVariable int studentId,Student student) {

		studentDao.deleteStudent(studentId);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	
/* update single student record */
	
	@RequestMapping(value = "/updateRecord", method = RequestMethod.PUT)
	public ResponseEntity updateStudent(@RequestBody Student student) {

		studentDao.updateStudent(student);
		return new ResponseEntity(HttpStatus.OK);
	}
	

}
